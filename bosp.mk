
ifdef CONFIG_EXTERNAL_FEEDGNUPLOT

# Targets provided by this project
.PHONY: feedgnuplot clean_feedgnuplot

# Add this to the "external" target
external: feedgnuplot
clean_external: clean_feedgnuplot

MODULE_DIR_FEEDGNUPLOT=external/tools/feedgnuplot

.PHONY: feedgnuplot clean_feedgnuplot
feedgnuplot: setup $(BUILD_DIR)/bin/feedgnuplot
$(BUILD_DIR)/bin/feedgnuplot:
	@echo
	@echo "==== Building FeedGNUPlot Tool v1.19 ===="
	@cd $(MODULE_DIR_FEEDGNUPLOT) && \
		perl Makefile.PL prefix=$(BUILD_DIR) && \
			make install
	@echo

clean_feedgnuplot:
	@echo "==== Clean-up FeedGNUPlot Tool v1.19 ===="
	@cd $(MODULE_DIR_FEEDGNUPLOT) && \
		{ [ ! -f Makefile ] || make distclean ;}
	@rm -f $(BUILD_DIR)/bin/feedgnuplot
	@echo

else # CONFIG_EXTERNAL_FEEDGNUPLOT

feedgnuplot:
	$(warning external FeedGNUPlot module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_FEEDGNUPLOT

